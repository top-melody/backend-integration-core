default: code-fix

code-fix:
	docker run --rm -v `pwd`/src:/data/src cytopia/php-cs-fixer fix .
