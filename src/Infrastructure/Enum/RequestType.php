<?php

namespace IntegrationCore\Infrastructure\Enum;

enum RequestType: string
{
    case GetStream = 'getStream';
    case Auth = 'auth';
    case GetUserTrackList = 'getUserTrackList';
    case GetUserInfo = 'getUserInfo';
}
