<?php

namespace IntegrationCore\Infrastructure\Component;

use IntegrationCore\Infrastructure\Enum;

interface IntegrationOperation
{
    public function buildRequest(): void;

    public function sendRequest(): void;

    /**
     * @return string[]
     */
    public function getErrors(): array;

    public function getType(): Enum\RequestType;

    public function getRequestUrl(): ?string;

    public function getRawRequest(): ?string;

    public function getRawResponse(): ?string;

    /**
     * @return array<string, string>
     */
    public function getRequestHeaders(): array;

    /**
     * @return array<string, string>
     */
    public function getResponseHeaders(): array;

    public function getResponse(): mixed;
}
