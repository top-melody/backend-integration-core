<?php

namespace IntegrationCore\Meta;

interface ServiceInterface
{
    public function service();
}
