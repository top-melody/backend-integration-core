<?php

namespace IntegrationCore\Domain\Service;

use DateTimeImmutable;
use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Domain\DTO;
use IntegrationCore\Infrastructure\Component as InfrastructureComponent;
use IntegrationCore\Meta\ServiceInterface;
use Throwable;

class IntegrationOperationExecutor implements ServiceInterface
{
    private ?DateTimeImmutable $executionStartDateTime = null;
    private ?DateTimeImmutable $executionEndDateTime = null;
    private ?string $exceptionMessage = null;

    public function __construct(
        readonly private InfrastructureComponent\IntegrationOperation $integrationOperation,
    ) {
    }

    public function service(): DataProvider\OperationResult
    {
        $this->integrationOperation->buildRequest();
        $this->executionStartDateTime = new DateTimeImmutable();

        try {
            $this->integrationOperation->sendRequest();
        } catch (Throwable $exception) {
            $this->exceptionMessage = $exception->getMessage();
        } finally {
            $this->executionEndDateTime = new DateTimeImmutable();
        }

        $result = new DTO\OperationResult();
        $result->response = $this->integrationOperation->getResponse();
        $result->log = $this->buildLog();

        return $result;
    }

    private function buildLog(): DataProvider\Log
    {
        $errors = $this->exceptionMessage ? [$this->exceptionMessage] : $this->integrationOperation->getErrors();

        $dto = new DTO\Log();
        $dto->isSuccess = !$errors;
        $dto->errors = $errors;
        $dto->type = $this->integrationOperation->getType();
        $dto->requestUrl = $this->integrationOperation->getRequestUrl();
        $dto->rawRequest = $this->integrationOperation->getRawRequest();
        $dto->rawResponse = $this->integrationOperation->getRawResponse();
        $dto->requestHeaders = $this->integrationOperation->getRequestHeaders();
        $dto->responseHeaders = $this->integrationOperation->getResponseHeaders();
        $dto->executionStartDateTime = $this->executionStartDateTime;
        $dto->executionEndDateTime = $this->executionEndDateTime;

        return $dto;
    }
}
