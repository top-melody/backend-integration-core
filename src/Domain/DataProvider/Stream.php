<?php

namespace IntegrationCore\Domain\DataProvider;

interface Stream
{
    public function read(int $bytes): string;
    public function isEndOfFile(): bool;
}
