<?php

namespace IntegrationCore\Domain\DataProvider;

interface OperationResult
{
    public function getResponse(): mixed;
    public function getLog(): Log;
}
