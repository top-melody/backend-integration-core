<?php

namespace IntegrationCore\Domain\DataProvider;

use DateTimeImmutable;
use IntegrationCore\Infrastructure\Enum;

interface Log
{
    public function isSuccess(): bool;
    public function getErrors(): array;
    public function getType(): Enum\RequestType;
    public function getRequestUrl(): ?string;
    public function getRawRequest(): ?string;
    public function getRawResponse(): ?string;
    public function getRequestHeaders(): array;
    public function getResponseHeaders(): array;
    public function getExecutionStartDateTime(): ?DateTimeImmutable;
    public function getExecutionEndDateTime(): ?DateTimeImmutable;
}
