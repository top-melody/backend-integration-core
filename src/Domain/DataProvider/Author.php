<?php

namespace IntegrationCore\Domain\DataProvider;

interface Author
{
    public function getName(): ?string;
}
