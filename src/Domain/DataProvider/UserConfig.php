<?php

namespace IntegrationCore\Domain\DataProvider;

interface UserConfig
{
    /**
     * @param array<mixed, mixed> $userConfig
     */
    public function __construct(array $userConfig);

    public function setUserUrl(?string $userUrl): self;

    public function getUserUrl(): ?string;
    public function toArray(): array;
}
