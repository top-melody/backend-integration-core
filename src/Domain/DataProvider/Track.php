<?php

namespace IntegrationCore\Domain\DataProvider;

interface Track
{
    public function getName(): ?string;

    public function getDuration(): ?int;

    public function getDownloadUrl(): ?string;

    /**
     * @return Author[]
     */
    public function getAuthors(): array;
}
