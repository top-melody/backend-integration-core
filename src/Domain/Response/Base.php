<?php

namespace IntegrationCore\Domain\Response;

use IntegrationCore\Domain\DataProvider;

interface Base
{
    /**
     * @return DataProvider\Log[]
     */
    public function getLogList(): array;

    /**
     * @param DataProvider\Log[] $logList
     */
    public function addLogList(array $logList): self;

    /**
     * @return string[]
     */
    public function getErrorList(): array;

    public function getSuccess(): bool;
}
