<?php

namespace IntegrationCore\Domain\Response;

use IntegrationCore\Domain\DataProvider;

interface TrackList extends Base
{
    /**
     * @return DataProvider\Track[]
     */
    public function getTrackList(): array;
}
