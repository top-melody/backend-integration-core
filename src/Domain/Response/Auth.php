<?php

namespace IntegrationCore\Domain\Response;

interface Auth extends Base
{
    /**
     * @return array<mixed, mixed>
     */
    public function getCommonConfig(): array;
}
