<?php

namespace IntegrationCore\Domain\Response;

interface GetUserInfo extends Base
{
    /**
     * @return array<mixed, mixed>
     */
    public function getUserConfig(): array;
}
