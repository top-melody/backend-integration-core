<?php

namespace IntegrationCore\Domain\Response;

use IntegrationCore\Domain\DataProvider;

interface GetStream extends Base
{
    public function getStream(): ?DataProvider\Stream;
    public function getSizeInBytes(): ?int;
    public function getMimeType(): ?string;
}
