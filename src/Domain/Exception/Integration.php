<?php

namespace IntegrationCore\Domain\Exception;

use Exception;
use IntegrationCore\Domain\DataProvider;

abstract class Integration extends Exception
{
    /**
     * @var DataProvider\Log[]
     */
    private array $logs;

    /**
     * @param DataProvider\Log[] $logs
     */
    public function __construct(array $logs = [])
    {
        parent::__construct('', 0, null);
        $this->logs = $logs;
    }

    /**
     * @return DataProvider\Log[]
     */
    public function getLogs(): array
    {
        return $this->logs;
    }
}
