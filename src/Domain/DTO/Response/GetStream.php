<?php

namespace IntegrationCore\Domain\DTO\Response;

use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Domain\Response;

class GetStream extends Base implements Response\GetStream
{
    public ?DataProvider\Stream $stream = null;
    public ?int $sizeInBytes = null;
    public ?string $mimeType = null;

    public function getStream(): ?DataProvider\Stream
    {
        return $this->stream;
    }

    public function getSizeInBytes(): ?int
    {
        return $this->sizeInBytes;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }
}
