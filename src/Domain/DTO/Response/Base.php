<?php

namespace IntegrationCore\Domain\DTO\Response;

use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Domain\Response;

abstract class Base implements Response\Base
{
    /**
     * @var DataProvider\Log[]
     */
    public array $logList = [];
    public array $errorList = [];


    /**
     * @return DataProvider\Log[]
     */
    public function getLogList(): array
    {
        return $this->logList;
    }

    public function addLogList(array $logList): self
    {
        $this->logList = array_merge($this->logList, $logList);

        return $this;
    }

    public function getErrorList(): array
    {
        return $this->errorList;
    }

    public function getSuccess(): bool
    {
        return empty($this->errorList);
    }
}
