<?php

namespace IntegrationCore\Domain\DTO\Response;

use IntegrationCore\Domain\Response;

class Auth extends Base implements Response\Auth
{
    public array $commonConfig = [];

    /**
     * @return array<mixed, mixed>
     */
    public function getCommonConfig(): array
    {
        return $this->commonConfig;
    }
}
