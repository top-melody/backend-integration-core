<?php

namespace IntegrationCore\Domain\DTO\Response;

use IntegrationCore\Domain\Response;
use IntegrationCore\Domain\DataProvider;

class TrackList extends Base implements Response\TrackList
{
    public array $trackList = [];

    /**
     * @return DataProvider\Track[]
     */
    public function getTrackList(): array
    {
        return $this->trackList;
    }
}
