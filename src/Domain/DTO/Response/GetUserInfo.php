<?php

namespace IntegrationCore\Domain\DTO\Response;

use IntegrationCore\Domain\Response;

class GetUserInfo extends Base implements Response\GetUserInfo
{
    public array $userConfig = [];

    /**
     * @return array<mixed, mixed>
     */
    public function getUserConfig(): array
    {
        return $this->userConfig;
    }
}
