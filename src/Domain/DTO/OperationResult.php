<?php

namespace IntegrationCore\Domain\DTO;

use IntegrationCore\Domain\DataProvider;

class OperationResult implements DataProvider\OperationResult
{
    public mixed $response = null;
    public ?DataProvider\Log $log = null;

    public function getResponse(): mixed
    {
        return $this->response;
    }

    public function getLog(): DataProvider\Log
    {
        return $this->log;
    }
}
