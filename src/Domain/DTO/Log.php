<?php

namespace IntegrationCore\Domain\DTO;

use DateTimeImmutable;
use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Infrastructure\Enum;

class Log implements DataProvider\Log
{
    public bool $isSuccess = false;
    public array $errors = [];
    public ?Enum\RequestType $type = null;
    public ?string $requestUrl = null;
    public ?string $rawRequest = null;
    public ?string $rawResponse = null;
    public array $requestHeaders = [];
    public array $responseHeaders = [];
    public ?DateTimeImmutable $executionStartDateTime = null;
    public ?DateTimeImmutable $executionEndDateTime = null;

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getType(): Enum\RequestType
    {
        return $this->type;
    }

    public function getRequestUrl(): ?string
    {
        return $this->requestUrl;
    }

    public function getRawRequest(): ?string
    {
        return $this->rawRequest;
    }

    public function getRawResponse(): ?string
    {
        return $this->rawResponse;
    }

    public function getRequestHeaders(): array
    {
        return $this->requestHeaders;
    }

    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    public function getExecutionStartDateTime(): ?DateTimeImmutable
    {
        return $this->executionStartDateTime;
    }

    public function getExecutionEndDateTime(): ?DateTimeImmutable
    {
        return $this->executionEndDateTime;
    }
}
