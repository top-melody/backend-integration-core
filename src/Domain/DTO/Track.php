<?php

namespace IntegrationCore\Domain\DTO;

use IntegrationCore\Domain\DataProvider;

class Track implements DataProvider\Track
{
    /**
     * Массив авторов
     * @var DataProvider\Author[]
     */
    public array $authors = [];

    /**
     * Название (без автора)
     */
    public ?string $name = null;

    /**
     * Длительность в секундах
     */
    public ?int $duration = null;

    /**
     * Ссылка на скачивание
     */
    public ?string $downloadUrl = null;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function getDownloadUrl(): ?string
    {
        return $this->downloadUrl;
    }

    public function getAuthors(): array
    {
        return $this->authors;
    }
}
