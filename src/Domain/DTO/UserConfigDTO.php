<?php

namespace IntegrationCore\Domain\DTO;

use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Domain\DataProvider\UserConfig;

class UserConfigDTO implements DataProvider\UserConfig
{
    public function __construct(
        protected array $userConfig = []
    ) {}

    public function setUserUrl(?string $userUrl): DataProvider\UserConfig
    {
        $this->userConfig['userUrl'] = $userUrl;

        return $this;
    }

    public function getUserUrl(): ?string
    {
        return $this->userConfig['userUrl'] ?? null;
    }

    public function toArray(): array
    {
        return $this->userConfig;
    }
}