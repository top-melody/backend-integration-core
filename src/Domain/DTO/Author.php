<?php

namespace IntegrationCore\Domain\DTO;

use IntegrationCore\Domain\DataProvider;

class Author implements DataProvider\Author
{
    /**
     * Имя
     */
    public ?string $name = null;

    public function getName(): ?string
    {
        return $this->name;
    }
}
