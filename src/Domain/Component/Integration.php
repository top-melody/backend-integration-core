<?php

namespace IntegrationCore\Domain\Component;

use IntegrationCore\Domain\Response;

interface Integration
{
    public function __construct(array $commonConfig, array $userConfig = []);

    public function auth(): Response\Auth;

    public function getStream(string $downloadUrl): Response\GetStream;

    /**
     * @return \Generator<Response\TrackList>
     */
    public function getUserTrackList(): \Generator;

    public function getUserInfo(): Response\GetUserInfo;
}
